package cz.sda.task14;

import java.util.Scanner;

/**
 * Write an application that reads two lowercase letters of the Latin alphabet (type char) and calculates how many
 * characters is there in the alphabet between given letters. Hint - use the ASCII code table and treat the characters
 * as int numbers
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter first letter: ");
        char char1 = scanner.nextLine().charAt(0);
        if (!Character.isLowerCase(char1)) {
            System.out.println("Entered character is not a lower case letter! Exiting program...");
            return;
        }
        System.out.print("Please enter second letter: ");
        char char2 = scanner.nextLine().charAt(0);
        if (!Character.isLowerCase(char2)) {
            System.out.println("Entered character is not a lower case letter! Exiting program...");
            return;
        }
        // calculate difference between character codes
        int count = Math.abs(char1 - char2);
        // reduce count by 1 to get the actual number of characters
        count -= 1;
        // find max between calculated count and 0 - solves the situation with same characters which results in -1
        count = Math.max(count, 0);
        System.out.println("There are " + count + " letters between '" + char1 + "' and '" + char2 + "'.");
    }
}
