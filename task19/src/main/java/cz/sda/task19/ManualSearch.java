package cz.sda.task19;

/**
 * Write an application that consists of few classes:
 * <p>
 * a. Author class, representing an author – poem writer, which consists of fields surname and nationality (both of type
 * String)
 * <p>
 * b. Poem class, representing poem, which consists of fields creator (type Author) and stropheNumbers (type int –
 * numbers of strophes in poem)
 * <p>
 * c. Main class, with main method, inside which you will: i. Create three instances of Poem class, fill them with data
 * (using constructor and/or setters) and store them in array ii. Write a surname of an author, that wrote a longest
 * poem (let your application calculate it!)
 */
public class ManualSearch {
    public static void main(String[] args) {
        Poem[] poems = new Poem[3];
        poems[0] = new Poem(new Author("Blake", "Great Britain"), 10);
        poems[1] = new Poem(new Author("Aaronovitch", "Great Britain"), 15);
        poems[2] = new Poem(new Author("Lasica", "Slovak"), 32);
        Poem longestPoem = poems[0];
        for (Poem poem : poems) {
            longestPoem = poem.getStropheNumbers() > longestPoem.getStropheNumbers()
                    ? poem
                    : longestPoem;
        }
        System.out.println(longestPoem.getCreator().getSurname());
    }
}
