package cz.sda.task18;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Write an application that reads a text from the user (type String) and checks whether the user sneezed, i.e. whether
 * the text equals "achooo!" with one or more letter 'o' at the end of the expression (so both "acho!" and "achooooooo!"
 * are correct). Hint: use a regular expression with the appropriate quantifier.
 */
public class MatcherSolution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        Matcher matcher = Pattern.compile("acho+!").matcher(input);
        if (matcher.matches()) {
            System.out.println("Gesundheit!");
        } else {
            System.out.println("Wat?");
        }
    }
}
