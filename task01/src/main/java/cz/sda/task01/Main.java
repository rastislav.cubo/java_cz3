package cz.sda.task01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float diameter = scanner.nextFloat();

        System.out.println("Perimeter of the circle is: " + perimeter(diameter));
    }

    public static double perimeter(float diameter) {
        return Math.PI * diameter;
    }
}
