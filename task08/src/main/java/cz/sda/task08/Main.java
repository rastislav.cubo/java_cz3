package cz.sda.task08;

import java.util.Scanner;

/**
 * Write an application that implements a simple calculator. The application should:
 * a. read first number (type float)
 * b. read one of following symbols: + - / *
 * c. read second number (type float)
 * d. return a result of given mathematical operation
 * If the user provides a symbol other than supported, the application should print "Invalid
 * symbol". If the entered action cannot be implemented (i.e. it is inconsistent with the
 * principles of mathematics), the application should print "Cannot calculate".
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number: ");
        float a = scanner.nextFloat();
        System.out.println("Enter operator");
        String operator = scanner.next();
        System.out.println("Enter second number: ");
        float b = scanner.nextFloat();
        float result = 0;

        switch (operator) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                if (b == 0) {
                    System.out.println("Cannot calculate.");
                    return;
                } else {
                    result = a / b;
                }
                break;
            default:
                System.out.println("Invalid symbol");
                return;
        }
        System.out.println("Result of the operation: " + result);
    }
}
