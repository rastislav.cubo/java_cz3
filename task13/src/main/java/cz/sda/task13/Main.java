package cz.sda.task13;

import java.util.Scanner;

/**
 * Write an application that "stutters", that is, reads the user's text (type String), and prints the given text, in
 * which each word is printed twice. For example, for the input: "This is my test" the application should print "This
 * This is is my my test test".
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        for (String word : text.split(" ")) {
            System.out.print(word + " " + word + " ");
        }
    }
}
