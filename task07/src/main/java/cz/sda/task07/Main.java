package cz.sda.task07;

import java.util.Scanner;

/**
 * Write an application that will take a positive number from the user (type int) and
 * calculate the Fibonacci number at the indicated index. For example, if the number equals
 * 5, your program should print the fifth Fibonacci number. In Fibonacci sequence, each
 * number is the sum of the two preceding ones. For example, the first few Fibonacci
 * numbers are:
 * 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the index of the fibonacci number you want to know: ");
        int count = scanner.nextInt();
        int previous = 0;
        int fibonacci = 1;

        for (int i = 1; i < count; i++) {
            int tmp = fibonacci;
            fibonacci += previous;
            previous = tmp;

        }
        System.out.println("Fibonacci's number at index " + count + " is " + fibonacci);
    }
}
