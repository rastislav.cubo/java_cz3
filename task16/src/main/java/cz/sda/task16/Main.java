package cz.sda.task16;

import java.util.Scanner;

/**
 * Write an application that takes 10 numbers from the user (type int) and write the length
 * of the longest such subsequence of these numbers, which is increasing.
 * For example, for the numbers: "1, 3, 8, 4, 2, 5, 6, 11, 13, 7" the program should write "5"
 * as the length of the longest increasing subsequence (underlined in the example).
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int currentNumber,
                previousNumber = 1,
                longestSequence = 0,
                currentSequence = 0;
        for (int i = 0; i < 10; i++) {
            currentNumber = scanner.nextInt();
            if (currentNumber > previousNumber) {
                currentSequence++;
                longestSequence = Math.max(currentSequence, longestSequence);
            } else {
                currentSequence = 1;
            }
            previousNumber = currentNumber;
        }
        System.out.println(longestSequence);
    }
}