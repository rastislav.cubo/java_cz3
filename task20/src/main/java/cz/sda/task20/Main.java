package cz.sda.task20;

import java.util.Random;
import java.util.Scanner;

/**
 * Write an application that will play "too much, too little" game with you. At the beginning
 * the application should randomly choose a number from 0 to 100 (hint: use the
 * Random.nextInt() method) and wait for the user to enter a number. If the user gives a
 * number greater than the number chosen by the computer, your application should print
 * "too much" and wait for the next number. If the user gives a smaller number, the
 * application should print "not enough" and wait for the next number in the same way. If
 * the user provides the exact value, the application should print the word
 * "Congratulations!" and finish.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int secret = random.nextInt(100);
        int guess = 0;
        while (guess != secret) {
            guess = scanner.nextInt();
            if (guess > secret) {
                System.out.println("Too much!");
            } else if (guess < secret) {
                System.out.println("Not enough!");
            } else {
                System.out.println("Congratulation!");
            }
        }
    }
}
