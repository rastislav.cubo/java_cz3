package cz.sda.task06;

import java.util.Scanner;

/**
 * Write an application that takes a number n from the user (type int) and calculates the sum of the harmonic series
 * from 1 to n, according to the formula below: <br/> Hn = 1 + 1/2 + 1/3 + 1/4 + ... + 1/n
 */
public class ForCycleSolution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double result = 0;
        for (double i = 1; i <= n; i++) {
            result += 1 / i;
        }
        System.out.println("Result is " + result);
    }
}
