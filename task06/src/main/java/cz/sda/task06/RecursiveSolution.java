package cz.sda.task06;

import java.util.Scanner;

/**
 * Write an application that takes a number n from the user (type int) and calculates the sum of the harmonic series
 * from 1 to n, according to the formula below: <br/> Hn = 1 + 1/2 + 1/3 + 1/4 + ... + 1/n
 */
public class RecursiveSolution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("Result is " + harmonicSeries(n));
    }

    public static double harmonicSeries(int n) {
        return n == 1 ? 1 : 1d / n + harmonicSeries(n - 1);
    }
}
