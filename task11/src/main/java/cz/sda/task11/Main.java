package cz.sda.task11;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

/**
 * Write an application that will read texts (variables of the String type) until the user gives
 * the text "Enough!" and then writes the longest of the given texts (not including the text
 * "Enough!"). If the user does not provide any text, write "No text provided".
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter text: ");
        String input = scanner.nextLine();
        String longest = "";
        while (!"Enough!".equals(input)) {
            longest = input.length() > longest.length() ? input : longest;
            System.out.print("Enter text: ");
            input = scanner.nextLine();
        }
        if (longest.isEmpty()) {
            System.out.println("No text provided");
        } else {
            System.out.println(longest);
        }
    }
}