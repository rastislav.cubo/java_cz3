package cz.sda.task09;

import java.util.Scanner;

public class OptionOne {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        printWave(length);
    }

    private static void printWave(int length) {
        for (int i = 1; i <= 4; i++) {
            printLine(i, length);
        }
    }

    private static void printLine(int line, int length) {
        int edgeSpaces = line - 1;
        int midSpaces = 8 - line * 2;
        for (int position = line; position <= length; ) {
            printSpaces(edgeSpaces);
            printStar();
            printSpaces(midSpaces);
            position += midSpaces + 1;
            if (position <= length) {
                printStar();
            } else {
                break;
            }
            printSpaces(edgeSpaces);
            position += 2 * edgeSpaces + 1;
        }
        System.out.println();
    }

    private static void printSpaces(int count) {
        System.out.print(" ".repeat(count));
    }

    private static void printStar() {
        System.out.print('*');
    }
}
