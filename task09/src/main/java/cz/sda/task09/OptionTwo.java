package cz.sda.task09;

import java.util.Scanner;

public class OptionTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int height = scanner.nextInt();
        printWave(length, height);
    }

    private static void printWave(int length, int height) {
        for (int line = 1; line <= height; line++) {
            printLine(line, length, 2 * height);
        }
    }

    private static void printLine(int line, int length, int lineFactor) {
        for (int column = 1; column <= length; column++) {
            int mod = column % lineFactor;
            if (mod == line || mod == (lineFactor - (line - 1)) % lineFactor) {
                System.out.print('*');
            } else {
                System.out.print(' ');
            }
        }
        System.out.print('\n');
    }
}
