package cz.sda.task12;

import java.util.Scanner;

/**
 * Write an application that reads a text from the user (type String) and counts a percentage of occurrences
 * of a space character.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Please enter any text:");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        int count = 0;
        for (char c : text.toCharArray()) {
            if (' ' == c) {
                count++;
            }
        }
        System.out.println("The percentage of occurrences of a space characters is: " + count * 100 / text.length() + " %");
    }
}

