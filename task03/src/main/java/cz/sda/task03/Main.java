package cz.sda.task03;

import java.util.Scanner;

/**
 * Write a program for solving a quadratic equation. The program should take three integers
 * (coefficients of the quadratic equation a, b, c) and calculate the roots of the
 * equation ax²+bx+c=0
 * If delta ∆ comes out negative, print "Delta negative" and exit the program.
 * Formulas you'll need:
 * ∆ = b² - 4ac
 * x1, x2 = (-b +/- √∆)/2a
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int delta = b * b - 4 * a * c;
        if (delta < 0) {
            System.out.println("Delta negative");
            return;
        }
        double x1 = (-b - Math.sqrt(delta))/2 * a;
        double x2 = (-b + Math.sqrt(delta))/2 * a;
        System.out.println(x1);
        System.out.println(x2);
    }
}
