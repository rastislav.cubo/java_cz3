package cz.sda.task15;

import java.util.Scanner;

/**
 * Write an application that reads from the user 10 arbitrarily large numbers (variables of type int) and prints those
 * that occurred at least twice.
 */
public class ArraySolution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];
        int[] found = new int[10];
        int f = 0;
        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < 9; i++) {
            for (int j = 9; j > i; j--) {
                if (array[i] == array[j]) {
                    boolean redundant = false;
                    for (int k = 0; k < f; k++) {
                        if (array[i] == found[k]) {
                            redundant = true;
                            break;
                        }
                    }
                    if (!redundant) {
                        found[f++] = array[i];
                    }
                    break;
                }
            }
        }
        for (int i = 0; i < f; i++) {
            System.out.print(found[i] + " ");
        }
    }
}
