package cz.sda.task15;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Write an application that reads from the user 10 arbitrarily large numbers (variables of type int) and prints those
 * that occurred at least twice.
 */
public class SetSolution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> inputs = new HashSet<>();
        Set<Integer> duplicates = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            int input = scanner.nextInt();
            if (!inputs.add(input)) {
                duplicates.add(input);
            }
        }
        for (Integer number : duplicates) {
            System.out.print(number + " ");
        }
    }
}
