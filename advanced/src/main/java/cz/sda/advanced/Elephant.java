package cz.sda.advanced;

public class Elephant extends Animal {
    public Elephant(String name) {
        super(name);
    }

    @Override
    public String getSound() {
        return "Elephant Tooot!";
    }
}
