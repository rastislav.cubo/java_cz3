package cz.sda.advanced;

import java.util.ArrayList;
import java.util.List;

public class ParallelMain {
    public static void main(String[] args) throws InterruptedException {
        List<String> list = new ArrayList<>();
        Thread t0 = new Thread(new MyThread(list));
        Thread t1 = new Thread(new MyThread(list));
        t0.start();
        t1.start();

        t0.join();
        t1.join();
        System.out.println(list.size());
        System.out.println(list);
    }

    public static class MyThread implements Runnable {

        private final List<String> letters;

        public MyThread(List<String> letters) {
            this.letters = letters;
        }

        @Override
        public void run() {
            for (int i = 1; i < 31; i++) {
                String letter = "" + i;
                synchronized (letters) {
                    if (letters.contains(letter)) {
                        System.out.println("Thread " + Thread.currentThread().getName() + "won't enter '" + letter + "'");
                    } else {
                        System.out.println("Thread " + Thread.currentThread().getName() + "entering '" + letter + "'");
                        letters.add(letter);
                    }
                }
            }
        }
    }
}
