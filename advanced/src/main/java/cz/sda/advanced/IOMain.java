package cz.sda.advanced;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class IOMain {
    public static void main(String[] args) throws IOException {

        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            in = new FileInputStream("user.txt");
            out = new FileOutputStream("user_output.txt");
            int previous = 0;
            int c;

            while ((c = in.read()) != -1) {
                if (previous >= 'A' && previous <= 'Z') {
                    out.write(c - ('a' - 'A'));
                } else {
                    out.write(c);
                }
                previous = c;
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
