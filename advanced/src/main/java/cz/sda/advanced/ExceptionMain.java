package cz.sda.advanced;

import java.util.List;

public class ExceptionMain {
    public static void main(String[] args) {
        List<Lion> lions = List.of(new Lion("Scar"), new Lion("Simba"));
        for (Lion lion : lions) {
            try {
                System.out.println(lion.getLionName());
            } catch (Exception e) {
                System.out.println("My name is " + lion.getName() + " and I am a coward because: " + e.getMessage());
            }
        }
    }
}
