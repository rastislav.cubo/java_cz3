package cz.sda.advanced;

import java.util.Iterator;

public class MyClass<T> implements Iterable<T>, Iterator<T> {

    private T one;
    private T two;
    private T three;

    private int current;

    public MyClass(T one, T two, T three) {
        this.one = one;
        this.two = two;
        this.three = three;
        this.current = 1;
    }

    @Override
    public Iterator<T> iterator() {
        current = 1;
        return this;
    }

    @Override
    public boolean hasNext() {
        return current <= 3;
    }

    @Override
    public T next() {
        switch (current++) {
            case 1:
                return one;
            case 2:
                return two;
            case 3:
                return three;
            default:
                return null;
        }
    }
}
