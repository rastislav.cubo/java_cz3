package cz.sda.advanced;

public enum Difficulty {
    EASY(0),
    MEDIUM(5),
    HARD(10);

    int level;

    Difficulty(int level) {
        this.level = level;
    }
}
