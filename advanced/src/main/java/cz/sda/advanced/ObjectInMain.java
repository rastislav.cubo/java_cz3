package cz.sda.advanced;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectInMain {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream in = null;
        ObjectInputStream ois = null;

        try {
            in = new FileInputStream("simba.txt");
            ois = new ObjectInputStream(in);
            Lion object = (Lion) ois.readObject();
            System.out.println(object);
        } finally {
            if (in != null) {
                in.close();
            }
            if (ois != null) {
                ois.close();
            }
        }
    }
}
