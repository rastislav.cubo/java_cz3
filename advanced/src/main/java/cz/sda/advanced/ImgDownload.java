package cz.sda.advanced;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImgDownload {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        var task = new ImgDownloader("https://img.wallpapersafari.com/desktop/1920/1080/45/17/zKlaQ3.jpg");

        ExecutorService svc = Executors.newSingleThreadExecutor();

        System.out.println(new Timestamp(Instant.now().toEpochMilli()) + " Starting download");

        var download = svc.submit(task);
        while (!download.isDone()) {
            System.out.println("Downloading...");
            Thread.sleep(100);
        }
        byte[] imgData = download.get();
        System.out.println(new Timestamp(Instant.now().toEpochMilli()) + " Finished download");
        System.out.println(new Timestamp(Instant.now().toEpochMilli()) + " Saving to file");
        new FileOutputStream("download.jpg").write(imgData);
        svc.shutdown();
    }

    static class ImgDownloader implements Callable<byte[]> {

        private final String urlAddress;

        public ImgDownloader(String url) {
            this.urlAddress = url;
        }

        @Override
        public byte[] call() throws Exception {
            URL url = new URL(urlAddress);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            byte[] result = out.toByteArray();
            out.close();
            in.close();
            return result;
        }
    }
}
