package cz.sda.advanced;

public class GenericsMain {
    public static void main(String[] args) {
        MyList<String> strings = new MyList<>(5);
        for (int i = 0; i < 5; i++) {
            strings.set(i, "Foo " + i);
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(strings.get(i));
        }

    }
}
