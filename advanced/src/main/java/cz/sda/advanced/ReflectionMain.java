package cz.sda.advanced;

import java.util.List;

public class ReflectionMain {
    public static void main(String[] args) {
        List<Animal> animals = List.of(new Elephant("Dumbo"), new Lion("Mufasa"), new Lion("Scar"));

        for (Animal animal : animals) {
            Class<? extends Animal> clazz = animal.getClass();
            Carnivore a = clazz.getAnnotation(Carnivore.class);
            if (a != null) {
                System.out.println("My name is " + animal.getName() + " and " + a.value());
            } else {
                System.out.println(animal.getName());
            }
        }
    }
}
