package cz.sda.advanced;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectOutMain {
    public static void main(String[] args) throws IOException {
        FileOutputStream out = null;
        ObjectOutputStream oos = null;

        Lion object = new Lion("Xerxes");


        try {
            out = new FileOutputStream("simba.txt");
            oos = new ObjectOutputStream(out);
            oos.writeObject(object);
        } finally {
            if (out != null) {
                out.close();
            }
            if (oos != null) {
                oos.close();
            }
        }
    }
}
