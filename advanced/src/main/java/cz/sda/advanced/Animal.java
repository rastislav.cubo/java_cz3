package cz.sda.advanced;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public abstract class Animal implements Serializable {

    private final String name;

    public Animal(String name) {
        this.name = name;
    }

    @NotNull()
    public abstract String getSound();

    public String getName() {
        return name;
    }
}
