package cz.sda.advanced;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Carnivore("I eat meat")
public class Lion extends Animal {
    public static final long serialVersionUID = 14;

    private List<Lion> prideMembers;
    private String position;

    public Lion(String name) {
        super(name);
        this.prideMembers = prideMembers;
    }

    public Lion(String name, String position) {
        super(name);
        prideMembers = new ArrayList<>();
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lion lion = (Lion) o;
        return Objects.equals(getName(), lion.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String getSound() {
        return "Lion Wrrr!";
    }

    public String getLionName() throws MyException {
        if ("Simba".equals(getName())) {
            throw new MyException("You ran away!");
        }
        return "I am a lion by the name " + getName();
    }
}
