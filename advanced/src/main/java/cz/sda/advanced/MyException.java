package cz.sda.advanced;

public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}
