package cz.sda.advanced;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Lion("Mustafa"));
        animals.add(new Elephant("Dumbo"));
        animals.add(new Animal("Spike") {
            @Override
            public String getSound() {
                return "Dog Wooof!";
            }
        });
        animals.add(new Animal("Tom") {
            @Override
            public String getSound() {
                return "Cat Meow!";
            }
        });
        for (Animal animal : animals) {
            printAnimal(animal);
        }

        var hard = Difficulty.HARD;
        System.out.println(Arrays.toString(Difficulty.values()));
        System.out.println(Difficulty.valueOf("EASY"));
        System.out.println(hard.name());
    }

    public static void printAnimal(Animal animal) {
        System.out.println(animal.getName());
        System.out.println(animal.getSound());
    }
}
