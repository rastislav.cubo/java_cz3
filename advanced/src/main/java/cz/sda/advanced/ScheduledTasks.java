package cz.sda.advanced;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledTasks {
    public static void main(String[] args) {
        ScheduledExecutorService ssvc = Executors.newSingleThreadScheduledExecutor();
        System.out.println("Scheduling 'booyah' with delay 1 second");
        ssvc.schedule(() -> System.out.println("booyah"), 1, TimeUnit.SECONDS);
        System.out.println("Scheduling 'cowabunga' with delay 3 second and period 5 seconds");
        ssvc.scheduleAtFixedRate(() -> System.out.println("cowabunga"), 3, 5, TimeUnit.SECONDS);
    }
}
