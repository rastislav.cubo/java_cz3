package cz.sda.advanced;

public class MyList<E> {

    private Object[] elements;

    public MyList(int size) {
        elements = new Object[size];
    }

    public E get(int i) {
        return (E) elements[i];
    }

    public void set(int i, E value) {
        elements[i] = value;
    }
}
