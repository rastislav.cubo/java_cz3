package cz.sda.advanced;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CollectionsMain {
    public static void main(String[] args) {
        var scar = new Lion("Scar", "betrayer");
        var mufasa = new Lion("Mufasa", "king");
        var simba = new Lion("Simba", "prince");
        Map<String, Lion> lions = new HashMap<>();
        lions.put(scar.getPosition(), scar);
        lions.put(mufasa.getPosition(), mufasa);
        lions.put(simba.getPosition(), simba);

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the kind of lion you want to find: ");
        System.out.println(lions.get(scanner.nextLine()).getName());
    }
}
