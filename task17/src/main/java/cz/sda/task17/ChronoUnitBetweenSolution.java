package cz.sda.task17;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

/**
 * Write an application that will read from the user the date of your next SDA classes and calculate how many days are
 * left to them. Hint: read the date as String and parse it to LocalDate. Get the current date using LocalDate.now()
 * method.
 */
public class ChronoUnitBetweenSolution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LocalDate nextDate, currentDate;
        currentDate = LocalDate.now();
        String input = scanner.nextLine();
        nextDate = LocalDate.parse(input);
        long difference = ChronoUnit.DAYS.between(currentDate, nextDate);
        System.out.println(difference);
    }
}
