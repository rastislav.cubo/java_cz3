package cz.sda.task02;

import java.util.Scanner;

/**
 * Write an application calculating BMI (Body Mass Index) and checking if itЀs optimal or not.
 * Your application should read two variables: weight (in kilograms, type float) and height
 * (in centimeters, type int). BMI should be calculated given following formula:
 * BMI = weight[kg]/height[m]²
 * The optimal BMI range is from 18.5 to 24.9, smaller or larger values are non-optimal
 * values. Your program should write "BMI optimal" or "BMI not optimal", according to the
 * assumptions above.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your weight: ");
        float weight = scanner.nextFloat();
        System.out.print("Enter your height: ");
        int height = scanner.nextInt();
        float bmi = calculateBmi(weight, height);
        if (18.5 > bmi || bmi > 24.9) {
            System.out.println("BMI not optimal");
        } else {
            System.out.println("BMI optimal");
        }
    }

    /**
     * Calculates BMI from given values.
     *
     * @param weight weight in Kg
     * @param height height in cm
     * @return calculated BMI
     */
    private static float calculateBmi(float weight, int height) {
        return calculateBmi(weight, height / 100f);
    }

    /**
     * Calculates BMI from given values.
     *
     * @param weight weight in Kg
     * @param height height in m
     * @return calculated BMI
     */
    private static float calculateBmi(float weight, float height) {
        return weight / (height * height);
    }
}


